﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Comunes.Interfaces
{
	public delegate void CurrentPathChangedEventHandler(string path);

	public interface IFileManager
	{
		public string CurrentPath { get; }
		public IList<IResultFS> List(bool includeSubFolder = false);
		public IResultFS CreateFile(string filename);
		public IResultFS CreateDirectory(string folder);
		public IResultFS RenameFile(string filename, string newFilename);
		public IResultFS SetCurrentPath(string path);
		public event CurrentPathChangedEventHandler CurrentPathChangedEvent;

	}
}
