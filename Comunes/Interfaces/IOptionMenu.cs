﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Comunes.Interfaces
{
	public interface IOptionMenu
	{
		public string Nombre { get; }
		public string Descripcion { get; }
		public List<string> Parametros { get; set; }
		public IComando Comando { get; }
		public IResultadoEjecucion EjecutarComando();
	}
}
