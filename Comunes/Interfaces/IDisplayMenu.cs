﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Comunes.Interfaces
{
	public delegate void OptionMenuPressedEventHandler(IOptionMenu itemMenu);
	public interface IDisplayMenu
	{
		public void CurrentPathChanged(string path);
		public void Welcome(string titulo, string path);
		public void WaitOptionMenu(IMenu menu);
		public void ShowHelper(IMenu menu);
		public void DisplayOptionMenuResult(IOptionMenu itemMenu, IResultadoEjecucion result);
		public void DisplayOptionMenuException(IOptionMenu itemMenu, Exception ex);

		public event OptionMenuPressedEventHandler OptionMenuPressedEvent;
	}
}
