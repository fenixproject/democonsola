﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Comunes.Interfaces
{
	public interface IResultFS
	{
		string Filename { get;  }
		string SubFolder { get; }
		string FolderPath { get; }
	}
}
