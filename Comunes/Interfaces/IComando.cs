﻿using System;

namespace Comunes.Interfaces
{
	public interface IComando
	{
		public bool EsAyuda { get; }
		public string NombreComando { get; }
		public string DescripcionComando { get; }
		public int CantidadParametrosRequeridos { get; }
		IResultadoEjecucion Ejecutar(string[] parametros);

	}
}
