﻿using Comunes.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Comunes
{
	public interface IResultadoEjecucion
	{
		public List<IResultFS> Resultado { get;}
		public bool HayError { get;  }
		public string DescripcionError { get; }
	}
}
