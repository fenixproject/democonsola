﻿using Comunes.Interfaces;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Linq;

namespace Comunes.Utiles
{
	public static class Extensiones
	{
		public static List<Type> GetTypesImplementsIComando(this Assembly ass )
		{

			var t = typeof(IComando);

			return ass.GetTypes().Where(r => t.IsAssignableFrom(r))?.ToList();
		}
	}
}
