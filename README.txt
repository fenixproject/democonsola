DemoConsola v0.1

Para su funcionamiento se requiere tener instalado Framework .Net Core v3.1 

Ejecuci�n: Dentro del directorio "AplicativoDemo" se encuentra el ejecutable "DemoConsola.exe", puede hacerle doble click con el mouse o simplemente "Enter" con el teclado.

Los comandos disponibles son:

�	touch [nombre de archivo] : Crea un archivo nuevo con el siguiente nombre y extensi�n.
�	mk [nombre de directorio] : Crea un directorio nuevo con el siguiente nombre.
�	mv [archivo1] [archivo2] : Cambia de nombre un archivo
�	mv [path1] [path2] : Mueve un archivo de directorio
�	ls : Muestra los archivos/carpetas que se encuentran en el directorio
�	ls -R: Muestra el contenido de todos los subdirectorios de forma recursiva.
�	cd [path]: Permite navegar entre los diferentes directorios
�	help [comando]: Permite ver un listado y que hace cada uno
