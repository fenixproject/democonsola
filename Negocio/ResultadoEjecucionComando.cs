﻿using Comunes;
using Comunes.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Negocio
{
	internal class ResultadoEjecucionComando : IResultadoEjecucion
	{
		public bool HayError { get; internal set; } = false;

		public string DescripcionError { get; internal set; } = "";

		public List<IResultFS> Resultado { get; internal set; } = new List<IResultFS> { };

	}
}
