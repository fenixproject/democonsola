﻿using Comunes.Interfaces;
using Servicios;
using System;
using System.Collections.Generic;
using System.Text;

namespace Negocio
{
	internal class FileManagerFactory
	{
		private static FileManager _fileManager = null;

		private FileManagerFactory()
		{

		}

		public static IFileManager InstanceFileManager
		{
			get
			{
				if (_fileManager == null)
					_fileManager = new FileManager();

				return _fileManager;
			}
		} 

	}
}
