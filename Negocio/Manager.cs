﻿using Comunes;
using Comunes.Interfaces;
using Comunes.Utiles;

using System;
using System.Reflection;

namespace Negocio
{
	public class Manager
	{
		public IDisplayMenu DisplayMenu { get; protected set; }
		public IMenu Menu { get; private set; }
		public Manager(IDisplayMenu displayMenu)
		{
			if (displayMenu == null)
				throw new NullReferenceException(string.Format("El parámetro '{0}' no puede ser Nulo", nameof(displayMenu)));

			DisplayMenu = displayMenu;
			DisplayMenu.OptionMenuPressedEvent += DisplayMenu_OptionMenuPressedEvent;

			FileManagerFactory.InstanceFileManager.CurrentPathChangedEvent += InstanceFileManager_CurrentPathChangedEvent;
			BuscarComandos();
		}

		private void InstanceFileManager_CurrentPathChangedEvent(string path)
		{
			DisplayMenu.CurrentPathChanged(path);
		}

		private void BuscarComandos()
		{
			Menu = new Menu();

			var tipos = this.GetType().Assembly.GetTypesImplementsIComando();
			if (tipos != null && tipos.Count > 0)
			{
				foreach(var c in tipos)
				{
					var instance = Activator.CreateInstance(c);
					if (instance != null)
					{
						OpcionMenu opcion = new OpcionMenu()
						{
							Comando = (IComando)instance
						};

						Menu.Opciones.Add(opcion);
					}
				}
			}
		}

		private void DisplayMenu_OptionMenuPressedEvent(IOptionMenu item)
		{
			try
			{
				if (item?.Comando?.EsAyuda == true)
				{
					DisplayMenu.ShowHelper(Menu);
				}
				else
				{
					var result = item.EjecutarComando();
					if (result == null)
						throw new NullReferenceException(string.Format("El resultado de la ejecución para la opción '{0}' no es válido.", item.Nombre));

					DisplayMenu.DisplayOptionMenuResult(item, result);
				}
				DisplayMenu.WaitOptionMenu(Menu);
			}
			catch(Exception ex)
			{
				DisplayMenu.DisplayOptionMenuException(item, ex);
				DisplayMenu.WaitOptionMenu(Menu);
			}
		}

		public void Iniciar()
		{
			try
			{
				var fm = FileManagerFactory.InstanceFileManager;

				DisplayMenu.Welcome("Bienvenido a la Consola Demo", fm.CurrentPath);
				DisplayMenu.WaitOptionMenu(Menu);
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}
	}
}
