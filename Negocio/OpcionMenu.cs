﻿using Comunes;
using Comunes.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Negocio
{
	internal class OpcionMenu : IOptionMenu
	{
		public string Nombre 
		{ 
			get
			{
				return Comando?.NombreComando?.ToLower();
			}
		}
		public string Descripcion
		{
			get
			{
				return Comando?.DescripcionComando;
			}
		}

		public List<string> Parametros { get; set; } = new List<string> { };

		public IComando Comando { get; internal set; }

		public IResultadoEjecucion EjecutarComando()
		{
			try
			{
				if (Comando == null)
					throw new NullReferenceException(string.Format("El comando '{0}' no es válido.", Nombre));

				return Comando.Ejecutar(Parametros?.ToArray());
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}
	}
}
