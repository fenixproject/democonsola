﻿using Comunes;
using Comunes.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Negocio.Comandos
{
	public sealed class RenombrarMoverArchivo : IComando
	{
		public bool EsAyuda { get; } = false;
		public string NombreComando { get; } = "mv";
		public string DescripcionComando { get; } = string.Format("mv [Arhivo1] [Archivo2] : cambia de nombre un archivo.{0}mv [path1] [path2] : Mueve un archivo de directorio.", Environment.NewLine);

		public int CantidadParametrosRequeridos { get; internal set; } = 2;


		public IResultadoEjecucion Ejecutar(string[] parametros)
		{
			var fm = FileManagerFactory.InstanceFileManager;
			
			ResultadoEjecucionComando resultado = new ResultadoEjecucionComando();

			try
			{
				if (parametros != null && parametros.Length >= CantidadParametrosRequeridos)
				{
					var res = fm.RenameFile(parametros[0], parametros[1]);

					resultado.HayError = res == null;
					resultado.DescripcionError = res != null ? "" : "No se puede renombrar o mover el archivo.";
					resultado.Resultado.Clear();
					if (res != null)
						resultado.Resultado.Add(res);
				}
				else
					throw new Exception("La cantidad de parámetros no es válida");

			}
			catch (Exception ex)
			{
				resultado.HayError = true;
				resultado.DescripcionError = ex.Message;
				resultado.Resultado.Clear();
			}

			return resultado;
		}
	}
}
