﻿using Comunes;
using Comunes.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Negocio.Comandos
{
	public sealed class CrearCarpeta : IComando
	{
		public bool EsAyuda { get; } = false;
		public string NombreComando { get; } = "mk";
		public string DescripcionComando { get; } = "mk [directorio] : Crea un directorio nuevo con el siguiente nombre.";

		public int CantidadParametrosRequeridos { get; internal set; } = 1;


		public IResultadoEjecucion Ejecutar(string[] parametros)
		{
			var fm = FileManagerFactory.InstanceFileManager;
			
			ResultadoEjecucionComando resultado = new ResultadoEjecucionComando();

			try
			{
				if (parametros != null && parametros.Length >= CantidadParametrosRequeridos)
				{
					var res = fm.CreateDirectory(parametros[0]);

					resultado.HayError = res == null;
					resultado.DescripcionError = res != null ? "" : "No se puede crear el archivo.";
					resultado.Resultado.Clear();
					if (res != null)
						resultado.Resultado.Add(res);
				}
				else
					throw new Exception("La cantidad de parámetros no es válida");

			}
			catch (Exception ex)
			{
				resultado.HayError = true;
				resultado.DescripcionError = ex.Message;
				resultado.Resultado.Clear();
			}

			return resultado;
		}
	}
}
