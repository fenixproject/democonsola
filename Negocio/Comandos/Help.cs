﻿using Comunes;
using Comunes.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Negocio.Comandos
{
	public sealed class Help : IComando
	{
		public bool EsAyuda { get; } = true;
		public string NombreComando { get; } = "help";
		public string DescripcionComando { get; } = "help [comando]: Permite ver un listado de todos los comandos soportados";

		public int CantidadParametrosRequeridos { get; internal set; } = 0;


		public IResultadoEjecucion Ejecutar(string[] parametros)
		{
			throw new NotImplementedException();
		}
	}
}
