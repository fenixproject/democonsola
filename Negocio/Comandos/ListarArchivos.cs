﻿using Comunes;
using Comunes.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Negocio.Comandos
{
	public sealed class ListarArchivos : IComando
	{
		public bool EsAyuda { get; } = false;
		public string NombreComando { get; } = "ls";
		public string DescripcionComando { get; } = string.Format("ls : Muestra los archivos/carpetas que se encuentran en el directorio.{0}ls -R : Muestra el contenido de todos los subdirectorios de forma recursiva.", Environment.NewLine);

		public int CantidadParametrosRequeridos { get; internal set; } = 0;

		public IResultadoEjecucion Ejecutar(string[] parametros)
		{
			var fm = FileManagerFactory.InstanceFileManager;
			
			ResultadoEjecucionComando resultado = new ResultadoEjecucionComando();

			try
			{
				IList<IResultFS> res = null;

				res = fm.List(parametros != null && parametros.Length > 0 && parametros[0]?.ToUpper() == "-R");

				resultado.HayError = res == null;
				resultado.DescripcionError = res != null ? "" : "No se puede crear el archivo.";
				
				if (res.Count > 0)
					resultado.Resultado.AddRange(res);

			}
			catch (Exception ex)
			{
				resultado.HayError = true;
				resultado.DescripcionError = ex.Message;
				resultado.Resultado.Clear();
			}

			return resultado;
		}
	}
}
