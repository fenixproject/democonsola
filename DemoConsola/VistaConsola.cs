﻿using Comunes;
using Comunes.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Negocio;
using System.IO;

namespace DemoConsola
{
	public class VistaConsola : IDisplayMenu
	{
		public event OptionMenuPressedEventHandler OptionMenuPressedEvent;
		private string CurrentPath;
		
		public VistaConsola()
		{

			CurrentPath = Directory.GetCurrentDirectory();

		}
		public void DisplayOptionMenuException(IOptionMenu itemMenu, Exception ex)
		{
			Console.WriteLine(ex.Message);
		}

		public void DisplayOptionMenuResult(IOptionMenu itemMenu, IResultadoEjecucion result)
		{
			if (result != null)
			{
				if (result.HayError)
				{
					Console.WriteLine(result.DescripcionError);
				}
				else
				{
					foreach(var i in result.Resultado)
					{
						if (i.SubFolder == Path.DirectorySeparatorChar.ToString())
							Console.WriteLine(i.Filename);
						else
							Console.WriteLine(Path.Combine(i.SubFolder, i.Filename));
					}
				}
			}
		}

		public void WaitOptionMenu(IMenu menu)
		{
			PrintPrompt(CurrentPath);
			var comando = Console.ReadLine();
			if (!string.IsNullOrEmpty(comando) && !string.IsNullOrWhiteSpace(comando))
			{
				var arrCmd = comando.Split(" ", StringSplitOptions.RemoveEmptyEntries);
				var cmd = arrCmd?.Length > 0 ? arrCmd[0] : "";
				string[] parametros = null;
				if (arrCmd?.Length > 1)
					parametros = arrCmd.Skip(1)?.ToArray();

				var opcionMenu = menu?.Opciones?.FirstOrDefault(r => r.Nombre.ToLower() == cmd.ToLower());
				if (opcionMenu != null)
				{
					opcionMenu.Parametros = new List<string> { };
					if (parametros != null && parametros.Length > 0)
						opcionMenu.Parametros.AddRange(parametros);

					this.OptionMenuPressedEvent?.Invoke(opcionMenu);
				}
				else
					PrintComandoInvalido(menu);
			}
			else
				PrintComandoInvalido(menu);
		}


		private void PrintComandoInvalido(IMenu menu)
		{
			Console.WriteLine(string.Format("Por favor ingrese un comando válido.{0}Para más información escriba el comando: help", Environment.NewLine));
			//PrintPrompt(CurrentPath);
			WaitOptionMenu(menu);
		}

		private void PrintPrompt(string path)
		{
			CurrentPath = path;
			Console.Write(string.Format("{0}\\>", path));
		}

		public void ShowHelper(IMenu menu)
		{
			foreach (var i in menu.Opciones)
			{
				Console.WriteLine(i.Descripcion);
			}
		}

		public void Welcome(string titulo, string path)
		{
			CurrentPath = path;
			Console.WriteLine(titulo);
			//PrintPrompt(CurrentPath);
		}

		public void CurrentPathChanged(string path)
		{
			CurrentPath = path;
		}
	}
}
