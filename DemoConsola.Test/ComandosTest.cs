using NUnit.Framework;
using Negocio.Comandos;

namespace DemoConsola.Test
{ 

	public class ComandosTest
	{
		private CrearArchivo touchCommand;
		private CurrentDirectory cdCommand;
		private ListarArchivos lsCommand;
		private RenombrarMoverArchivo mvCommand;
		private CrearCarpeta mkCommand;
		[SetUp]
		public void Setup()
		{
			touchCommand = new CrearArchivo();
			cdCommand = new CurrentDirectory();
			lsCommand = new ListarArchivos();
			mvCommand = new RenombrarMoverArchivo();
			mkCommand = new CrearCarpeta();
		}

		[Test]
		public void CrearArchivoTest()
		{
			System.Collections.Generic.List<string> parametros = new System.Collections.Generic.List<string>();

			var prueba1 = "prueba1.txt";

			parametros.Add(prueba1);

			var result = touchCommand.Ejecutar(parametros.ToArray());
			
			if (result == null || result.HayError)
			{
				if (result == null)
					Assert.Fail();
				else
					Assert.Fail(result.DescripcionError);
			}
			else
				Assert.IsTrue(result.Resultado?.Count > 0);
		}

		[Test]
		public void RenombrarArchivoTest()
		{
			System.Collections.Generic.List<string> parametros = new System.Collections.Generic.List<string>();

			var prueba1 = "prueba1.txt";
			var pruebaRen = "prueba_renombrado.txt";

			parametros.Add(prueba1);

			var result = touchCommand.Ejecutar(parametros.ToArray());

			parametros.Add(pruebaRen);

			result = mvCommand.Ejecutar(parametros.ToArray());

			if (result == null || result.HayError)
			{
				if (result == null)
					Assert.Fail();
				else
					Assert.Fail(result.DescripcionError);
			}
			else
				Assert.IsTrue(result.Resultado?.Count > 0);
		}

		[Test]
		public void MoverArchivoTest()
		{
			System.Collections.Generic.List<string> parametros = new System.Collections.Generic.List<string>();

			var carpetaPrueba = "carpetaPrueba";
			var prueba1 = "prueba1.txt";

			parametros.Add(carpetaPrueba);

			var result = mkCommand.Ejecutar(parametros.ToArray());

			parametros.Clear();

			parametros.Add(prueba1);

			result = touchCommand.Ejecutar(parametros.ToArray());

			var filePath = System.IO.Path.Combine(carpetaPrueba, prueba1);
			if (System.IO.File.Exists(filePath))
			{
				System.IO.File.Delete(filePath);
			}

			parametros.Clear();

			parametros.Add(prueba1);

			parametros.Add(carpetaPrueba);

			result = mvCommand.Ejecutar(parametros.ToArray());

			if (result == null || result.HayError)
			{
				if (result == null)
					Assert.Fail();
				else
					Assert.Fail(result.DescripcionError);
			}
			else
				Assert.IsTrue(result.Resultado?.Count > 0);
		}

		[Test]
		public void ListarArchivosTest()
		{
			System.Collections.Generic.List<string> parametros = new System.Collections.Generic.List<string>();


			var result = lsCommand.Ejecutar(parametros.ToArray());

			if (result == null || result.HayError)
			{
				if (result == null)
					Assert.Fail();
				else
					Assert.Fail(result.DescripcionError);
			}
			else
				Assert.IsTrue(result.Resultado != null);
		}

		[Test]
		public void ListarArchivosRecursivosTest()
		{
			System.Collections.Generic.List<string> parametros = new System.Collections.Generic.List<string>();

			parametros.Add("-R");

			var result = lsCommand.Ejecutar(parametros.ToArray());

			if (result == null || result.HayError)
			{
				if (result == null)
					Assert.Fail();
				else
					Assert.Fail(result.DescripcionError);
			}
			else
				Assert.IsTrue(result.Resultado != null);
		}

		[Test]
		public void EntrarEnDirectorioTest()
		{
			System.Collections.Generic.List<string> parametros = new System.Collections.Generic.List<string>();

			var carpetaPrueba = "carpetaPruebaCD";

			parametros.Add(carpetaPrueba);

			var result = mkCommand.Ejecutar(parametros.ToArray());

			result = cdCommand.Ejecutar(parametros.ToArray());

			if (result == null || result.HayError)
			{
				if (result == null)
					Assert.Fail();
				else
					Assert.Fail(result.DescripcionError);
			}
			else
				Assert.IsTrue(result.Resultado != null);
		}

		[Test]
		public void SalirDeDirectorioTest()
		{
			System.Collections.Generic.List<string> parametros = new System.Collections.Generic.List<string>();

			parametros.Add("..");

			var result = cdCommand.Ejecutar(parametros.ToArray());

			if (result == null || result.HayError)
			{
				if (result == null)
					Assert.Fail();
				else
					Assert.Fail(result.DescripcionError);
			}
			else
				Assert.IsTrue(result.Resultado != null);
		}

	}
}