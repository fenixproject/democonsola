﻿using Comunes.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Servicios
{
	public class FileManager : IFileManager
	{

		private string _currentPath = "";

		public event CurrentPathChangedEventHandler CurrentPathChangedEvent;

		public string CurrentPath 
		{
			get
			{
				return _currentPath;
			}
			private set
			{
				if (_currentPath != value)
				{
					_currentPath = value;
					CurrentPathChangedEvent?.Invoke(_currentPath);
				}
			} 
		}

		public FileManager()
		{
			CurrentPath = Directory.GetCurrentDirectory();
		}

		public FileManager(string initPath)
		{
			initPath = initPath.Replace("\"", "");
			CurrentPath = initPath;
			if (String.IsNullOrEmpty(CurrentPath) || String.IsNullOrWhiteSpace(CurrentPath) || !Directory.Exists(CurrentPath))
				CurrentPath = Directory.GetCurrentDirectory();
		}

		public IResultFS CreateFile(string filename)
		{
			try
			{
				filename = filename?.Replace("\"", "");

				var filePath = filename;

				var tmpFolder = Path.GetDirectoryName(filePath);

				if (string.IsNullOrEmpty(tmpFolder))
					filePath = Path.Combine(CurrentPath, filename);
	
				if (File.Exists(filePath))
					return new ResultFS()
					{
						Filename = Path.GetFileName(filePath),
						FolderPath = Path.GetDirectoryName(filePath),
						SubFolder = Path.DirectorySeparatorChar.ToString()
					};


				using (var f = File.Create(filePath))
				{
					if (File.Exists(filePath))
						return new ResultFS()
						{
							Filename = Path.GetFileName(filePath),
							FolderPath = Path.GetDirectoryName(filePath),
							SubFolder = Path.DirectorySeparatorChar.ToString()
						};
					else
						return null;

				}

			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public IResultFS CreateDirectory(string folder)
		{
			try
			{
				folder = folder?.Replace("\"", "");

				var filePath = folder;


				if (Directory.Exists(filePath))
					return new ResultFS()
					{
						Filename = "",
						FolderPath = Path.GetDirectoryName(filePath),
						SubFolder = filePath
					};


				var f = Directory.CreateDirectory(filePath);

				if (Directory.Exists(filePath))
					return new ResultFS()
					{
						Filename = "",
						FolderPath = Path.GetDirectoryName(filePath),
						SubFolder = filePath
					};
				else
					return null;


			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		private IList<IResultFS> ListFolder(string path, string subPath, bool includeSubFolder)
		{
			List<IResultFS> lista = new List<IResultFS> {  };
			try
			{
				string[] files = Directory.GetFiles(path);

				string[] directories = null;

				if (files != null && files.Length > 0)
				{
					
					foreach (var f in files)
					{
						lista.Add( 
								new ResultFS(){
										Filename=	Path.GetFileName(f), 
										SubFolder=	subPath,
										FolderPath =	path 
									}
								);
						
					}
				}

				if (includeSubFolder)
				{
					directories = Directory.GetDirectories(path);

					if (directories != null && directories.Length > 0)
					{
						foreach (var d in directories)
						{
							var dTmp = d.Split(Path.DirectorySeparatorChar, StringSplitOptions.RemoveEmptyEntries)?.Last();

							var spath = Path.Combine(path, dTmp);
							var lstTmp = ListFolder(spath, dTmp, includeSubFolder);
							if (lstTmp != null && lstTmp.Count > 0)
								lista.AddRange(lstTmp);
						}
					}
				}

			}
			catch (Exception ex)
			{
				throw ex;
			}

			return lista;
		}

		public IList<IResultFS> List(bool includeSubFolder = false)
		{
			try
			{
				return ListFolder(CurrentPath, Path.DirectorySeparatorChar.ToString(), includeSubFolder);
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public IResultFS RenameFile( string filename, string newFilename)
		{
			try
			{
				filename = filename?.Replace("\"", "");
				newFilename = newFilename?.Replace("\"", "");

				var filePath = filename;
				var newFilePath = newFilename;

				var tmpFolder = Path.GetDirectoryName(filePath);

				if (string.IsNullOrEmpty(tmpFolder))
					filePath = Path.Combine(CurrentPath, filename);

				if (!File.Exists(filePath))
					throw new FileNotFoundException(string.Format("El archivo '{0}' no existe.", filePath));

				tmpFolder = Path.GetDirectoryName(newFilePath);

				if (string.IsNullOrEmpty(tmpFolder))
					newFilePath = Path.Combine(CurrentPath, newFilename );

				var fileNameTmp = Path.GetFileName(filePath);

				string[] files = null;
				try
				{
					files = System.IO.Directory.GetFiles(newFilePath);
				}
				catch
				{
					files = null;
				}

				if (!newFilePath.EndsWith(fileNameTmp) && files != null )
					newFilePath = Path.Combine(newFilePath, fileNameTmp);

				
				if (File.Exists(newFilePath))
					File.Delete(newFilePath);

				File.Move(filePath, newFilePath);

				if (File.Exists(newFilePath))
					return new ResultFS()
					{
						Filename = Path.GetFileName(newFilePath),
						FolderPath = Path.GetDirectoryName(newFilePath),
						SubFolder = Path.DirectorySeparatorChar.ToString()
					};
				else
					return null;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public IResultFS SetCurrentPath(string path)
		{
			try
			{
				if (string.IsNullOrEmpty(path) || string.IsNullOrWhiteSpace(path))
					return null;

				path = path.Replace("\"", "");


				if (path != null && path == "..")
				{
					var pathTmp = "";
					var arrTmp = CurrentPath.Split(Path.DirectorySeparatorChar, StringSplitOptions.RemoveEmptyEntries);

					if (arrTmp != null)
					{
						for (int i = 0; i < arrTmp.Length - 1; i++)
						{
							pathTmp = Path.Combine(pathTmp, arrTmp[i]);
						}

						return SetCurrentPath(pathTmp);
					}
				}

				if (Directory.Exists(path))
				{
					path = Path.GetFullPath(path);
					CurrentPath = path;
					return new ResultFS()
					{
						Filename = "",
						FolderPath = path,
						SubFolder = Path.DirectorySeparatorChar.ToString()
					};
				}
				else
				{
					var arr = path.Split(Path.DirectorySeparatorChar, StringSplitOptions.RemoveEmptyEntries);
					if (arr != null && arr.Length == 1)
					{
						var pathTmp = Path.Combine(CurrentPath, path);
						return SetCurrentPath(pathTmp);
					}
					else
					{
						return null;
					}
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}
	}
}
