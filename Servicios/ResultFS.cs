﻿using Comunes.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Servicios
{
	public class ResultFS : IResultFS
	{		
		public string Filename { get; internal set; }

		public string SubFolder { get; internal set; }

		public string FolderPath { get; internal set; }
	}
}
